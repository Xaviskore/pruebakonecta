/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
   $('#frmProduct').submit(function(e){
       e.preventDefault();
       var form = $(this).serialize();
       console.log(form);
       $.ajax({
            url: "/pruebakonecta/controllers/process.php",
            type: "POST",
            data: {task:"newP",form:form},
            dataType: "json",
            success : function(result){
                if(result.status == "ok"){
                    alert(result.msg);
                    window.location.reload();
                }else{
                    alert(result.msg);
                }
            }
        });
   });
   
   $('#frmProductEdit').submit(function(e){
       e.preventDefault();
       var form = $(this).serialize();
       console.log(form);
       $.ajax({
            url: "/pruebakonecta/controllers/process.php",
            type: "POST",
            data: {task:"editP",form:form},
            dataType: "json",
            success : function(result){
                if(result.status == "ok"){
                    alert(result.msg);
                    window.location.href ="/pruebakonecta/views/productos/list.php";
                }else{
                    alert(result.msg);
                }
            }
        });
   });
   
   $('.delB').click(function(){
        var el = $(this).attr('data-id');
        let text = "Seguro que quiere eliminar ese producto?";
        if (confirm(text) == true) {
          $.ajax({
            url: "/pruebakonecta/controllers/process.php",
            type: "POST",
            data: {task:"deleteP",id:el},
            dataType: "json",
            success : function(result){
                if(result.status == "ok"){
                    alert(result.msg);
                    window.location.reload();
                }else{
                    alert(result.msg);
                }
            },
            });
        }   
   }); 
   
   $('.delVenta').click(function(){
        var el = $(this).attr('data-id');
        let text = "Seguro que quiere eliminar esta venta?";
        if (confirm(text) == true) {
          $.ajax({
            url: "/pruebakonecta/controllers/process.php",
            type: "POST",
            data: {task:"deleteV",id:el},
            dataType: "json",
            success : function(result){
                if(result.status == "ok"){
                    alert(result.msg);
                    window.location.reload();
                }else{
                    alert(result.msg);
                }
            },
            });
        }   
   }); 
});


