<?php
//Se definen rutas para poder manejar mas facil la inclusion de archivos
define('ROOT_PATH', '/pruebakonecta/');
define('ROOT_MPATH', $_SERVER['DOCUMENT_ROOT'].'/pruebakonecta/');
define('ASSETS_JS', ROOT_PATH.'assets/js/');
define('ASSETS_CCS', ROOT_PATH.'assets/css/');
define('CONTROLLERS_PATH', ROOT_MPATH.'controllers/');
define('MODELS_PATH', ROOT_MPATH.'models/');

//$_SERVER['DOCUMENT_ROOT'].
