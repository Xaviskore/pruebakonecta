<?php

Class DbModel{
 
    public $host;
    public $user;
    public $pass;
    public $db;
    
    public function __construct(){
        $this->host = "localhost";
        $this->user = "root";
        $this->pass = "";
    }
    
    public function connectDb(){
        try{
            $db = mysqli_connect($this->host, $this->user, $this->pass, 'konecta', '3306');
            mysqli_set_charset($db, 'utf8');
        }catch(Exception $e){
            $db = false;
            echo "Message ".$e->getMessage();
        }
        return $db;
    }
    
    public function disconnect($db){
        mysqli_close($db);
    }
    
}
