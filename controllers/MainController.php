<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
   include_once('../dirs.php');
}
include_once(MODELS_PATH.'DbModel.php');

Class MainController{
    
    public $db;
    public $model;
    
    public function __construct() {
        $this->model = new DbModel();
        $this->db = $this->model->connectDb();
    }
    
    public function getCategories(){
        $dbInstance = $this->db;
        if($dbInstance != false){
            $sql = "SELECT * FROM categoria";
            $query = mysqli_query($dbInstance, $sql);
            $num_rows = mysqli_num_rows($query);
            if($query && $num_rows > 0){
                $i = 0;
                while($row = mysqli_fetch_assoc($query)){
                    $data[$i]['id'] = $row['id'];
                    $data[$i]['nombre'] = $row['nombre'];
                    $i++;
                }
                $result = $data;
            }
        }
        return $result;
    }
    
    public function getProducts() {
        $dbInstance = $this->db;
        $stmt = mysqli_query($dbInstance,'SELECT * FROM `producto`');
        $productoData = array();
        while($data = mysqli_fetch_object($stmt)){
            $productoData[] = $data;
        }
        mysqli_free_result($stmt);
        mysqli_close($dbInstance);
        return $productoData;
    }
    
    public function newProduct($data) {
        $result = array("status" => "err", "msg" => "");
        $dbInstance = $this->db;
        $params = array();
        parse_str($data, $params);
        extract($params);
        if(!$this->existsR($referencia,0)){
            $stmt = mysqli_prepare($dbInstance,'INSERT INTO `producto`(`nombre`, `referencia`, `precio`, `peso`, `stock`, `categoria_id`) VALUES (?,?,?,?,?,?);');
            mysqli_stmt_bind_param($stmt, 'ssiiii', $nombre, $referencia, $precio, $peso, $stock, $categoria_id);
            if(mysqli_stmt_execute($stmt)){
                $result["status"] = "ok";
                $result["msg"] = "Producto creado con éxito";
            }else{
                $result["msg"] = "Ocurrió un error al crear el producto";
            }
            mysqli_stmt_close($stmt);
        }else{
            $result["msg"] = "Ya existe referencia de producto";
        }
        header('Content-type: application/json');
        echo json_encode($result);
    }
    
    public function editProduct($data) {
        $result = array("status" => "err", "msg" => "");
        $dbInstance = $this->db;
        $params = array();
        parse_str($data, $params);
        extract($params);
        if(!$this->existsR($referencia,1)){
            $stmt = mysqli_prepare($dbInstance,'UPDATE `producto` SET `nombre` = ?, `referencia` = ?, `precio` = ?, `peso` = ?, `stock` = ?, `categoria_id` = ? WHERE id = ?;');
            mysqli_stmt_bind_param($stmt, 'ssiiiii', $nombre, $referencia, $precio, $peso, $stock, $categoria_id, $id);
            if(mysqli_stmt_execute($stmt)){
                $result["status"] = "ok";
                $result["msg"] = "Producto actualizado con éxito";
            }else{
                $result["msg"] = "Ocurrió un error al actualizar el producto";
            }
            mysqli_stmt_close($stmt);
        }else{
            $result["msg"] = "Ya existe referencia de producto";
        }
        header('Content-type: application/json');
        echo json_encode($result);
    }
    
    public function deleteProduct($id) {
        $result = array("status" => "err", "msg" => "");
        $dbInstance = $this->db;
        $stmt = mysqli_prepare($dbInstance,'DELETE FROM `producto` WHERE `id` = ?;');
        mysqli_stmt_bind_param($stmt, 'i', $id);
        if(mysqli_stmt_execute($stmt)){
            $result["status"] = "ok";
            $result["msg"] = "Producto eliminado con éxito";
        }else{
            $result["msg"] = "Ocurrió un error al eliminar el producto";
        }
        mysqli_stmt_close($stmt);
        header('Content-type: application/json');
        echo json_encode($result);
    }
    
    public function getProduct($id) {
        $dbInstance = $this->db;
        $stmt = mysqli_prepare($dbInstance,'SELECT id,nombre,referencia,precio,peso,stock,categoria_id FROM `producto` WHERE id =?;');
        mysqli_stmt_bind_param($stmt, 'i', $id);
        mysqli_stmt_execute($stmt);
        $productoData = array();
        mysqli_stmt_bind_result($stmt,$id,$nombre,$referencia,$precio,$peso,$stock,$categoria_id);
        while (mysqli_stmt_fetch($stmt)) {
           $productoData['id'] = $id;
           $productoData['nombre'] = $nombre;
           $productoData['referencia'] = $referencia;
           $productoData['precio'] = $precio;
           $productoData['peso'] = $peso;
           $productoData['stock'] = $stock;
           $productoData['categoria_id'] = $categoria_id;
        }
        mysqli_stmt_close($stmt);
        return (object)$productoData;
    }
    
    public function getProductByRef($id) {
        $dbInstance = $this->db;
        $stmt = mysqli_prepare($dbInstance,'SELECT id,nombre,referencia,precio,stock FROM `producto` WHERE referencia =?;');
        mysqli_stmt_bind_param($stmt, 's', $id);
        mysqli_stmt_execute($stmt);
        $productoData = array();
        mysqli_stmt_bind_result($stmt,$id,$nombre,$referencia,$precio,$stock);
        while (mysqli_stmt_fetch($stmt)) {
           $productoData['id'] = $id;
           $productoData['nombre'] = $nombre;
           $productoData['referencia'] = $referencia;
           $productoData['precio'] = $precio;
           $productoData['stock'] = $stock;
        }
        mysqli_stmt_close($stmt);
        return (object)$productoData;
    }
    
    public function existsR($ref,$i) {
        $dbInstance = $this->db;
        $stmt = mysqli_prepare($dbInstance,'SELECT id FROM `producto` WHERE referencia =?;');
        mysqli_stmt_bind_param($stmt, 's', $ref);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);
        if(mysqli_stmt_num_rows($stmt) > $i){
            mysqli_stmt_close($stmt);
            return true;
        }else{
            return false;
        }
    }
    
    public function newSale($data,$session){
        $dbInstance = $this->db;
        $stmt = mysqli_prepare($dbInstance,'INSERT INTO `venta`(`total`) VALUES (?);');
        mysqli_stmt_bind_param($stmt, 's', $data);
        if(mysqli_stmt_execute($stmt)){
            $lastId = mysqli_stmt_insert_id($stmt);
            $saleID = $lastId === false ? 1 : $lastId;
            $stmt1 = mysqli_prepare($dbInstance,'INSERT INTO `producto_venta`(`producto_id`, `cantidad`, `venta_id`) VALUES (?, ?, ?);');
            $stmtStock = mysqli_prepare($dbInstance,'UPDATE `producto` SET `stock` = `stock` - ? WHERE id = ?;');
            foreach ($session as $producto){
                $total += $producto->total;
                mysqli_stmt_bind_param($stmt1, 'iii', $producto->id, $producto->cantidad, $saleID);
                mysqli_stmt_execute($stmt1);
                mysqli_stmt_bind_param($stmtStock, 'ii', $producto->cantidad, $producto->id);
                mysqli_stmt_execute($stmtStock);
            }
            return true;
        }else{
            return false;
        }
        mysqli_stmt_close($stmt);
        mysqli_stmt_close($stmt1);  
        mysqli_stmt_close($stmtStock);  
    }
    
    public function deleteSale($id){
        $result = array("status" => "err", "msg" => "");
        $dbInstance = $this->db;
        $stmt = mysqli_prepare($dbInstance,'DELETE FROM `venta` WHERE `id` = ?;');
        mysqli_stmt_bind_param($stmt, 'i', $id);
        if(mysqli_stmt_execute($stmt)){
            $result["status"] = "ok";
            $result["msg"] = "Venta eliminada con éxito";
        }else{
            $result["msg"] = "Ocurrió un error al eliminar la venta";
        }
        mysqli_stmt_close($stmt);
        header('Content-type: application/json');
        echo json_encode($result);
    }
    
    public function getSales() {
        $dbInstance = $this->db;
        $stmt = mysqli_query($dbInstance,"SELECT venta.total, venta.fecha_creacion, venta.id, GROUP_CONCAT(producto.referencia, '..', producto_venta.cantidad SEPARATOR '__') AS productos FROM venta
            INNER JOIN producto_venta ON producto_venta.venta_id = venta.id
            INNER JOIN producto ON producto.id = producto_venta.producto_id
            GROUP BY venta.id ORDER BY venta.id;");
        $saleData = array();
        while($data = mysqli_fetch_object($stmt)){
            $saleData[] = $data;
        }
        mysqli_free_result($stmt);
        mysqli_close($dbInstance);
        return $saleData;
    }
    
    public function getPIdBySQL($sql){
        $dbInstance = $this->db;
        $stmt = mysqli_query($dbInstance,$sql);
        $data = array();
        while($row = mysqli_fetch_object($stmt)){
            $data[] = $row;
        }
        mysqli_free_result($stmt);
        //mysqli_close($dbInstance);
        return $data;
    }
    
}

