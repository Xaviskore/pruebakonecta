<?php
include_once 'MainController.php';

/* AJAX check  */
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $class = new MainController();       
    if(isset($_POST['task']) && $_POST['task'] != ""){
        $task = $_POST['task'];
        switch ($task){
            case 'getCategories':
                $return = $class->getCategories();
                break;
            case 'newP':
                $return = $class->newProduct($_POST['form']);
                break;
            case 'editP':
                $return = $class->editProduct($_POST['form']);
                break;
            case 'deleteP':
                $return = $class->deleteProduct($_POST['id']);
                break;
            case 'deleteV':
                $return = $class->deleteSale($_POST['id']);
                break;
        }
    }
    //header("Content-Type: application/json", true);
    //echo json_encode($return); 
}

