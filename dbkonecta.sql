/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.4.22-MariaDB : Database - konecta
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`konecta` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `konecta`;

/*Table structure for table `categoria` */

DROP TABLE IF EXISTS `categoria`;

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `categoria` */

insert  into `categoria`(`id`,`nombre`) values 
(1,'Papeleria'),
(2,'Electronicos');

/*Table structure for table `producto` */

DROP TABLE IF EXISTS `producto`;

CREATE TABLE `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `referencia` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `precio` int(11) NOT NULL,
  `peso` int(5) NOT NULL,
  `stock` int(8) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `fk_categoria` (`categoria_id`),
  CONSTRAINT `fk_categoria` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `producto` */

insert  into `producto`(`id`,`nombre`,`referencia`,`precio`,`peso`,`stock`,`categoria_id`,`fecha_creacion`) values 
(2,'producto2','PF112',4555,12,2,1,'2022-01-27 08:39:43'),
(3,'proc2','PF113',45,12,10,1,'2022-01-27 08:40:02'),
(4,'proe','PF114',15,45,11,2,'2022-01-27 08:40:04'),
(5,'dsfaoi','PF115',12,2121,33,1,'2022-01-27 08:40:06'),
(6,'pske','PF116',212,213,209,1,'2022-01-27 08:40:09'),
(7,'rewr','PF117',23,3,1,2,'2022-01-27 08:40:16');

/*Table structure for table `producto_venta` */

DROP TABLE IF EXISTS `producto_venta`;

CREATE TABLE `producto_venta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `venta_id` int(11) NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `fk_venta` (`venta_id`),
  KEY `fk_producto` (`producto_id`),
  CONSTRAINT `fk_producto` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_venta` FOREIGN KEY (`venta_id`) REFERENCES `venta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `producto_venta` */

insert  into `producto_venta`(`id`,`producto_id`,`cantidad`,`venta_id`,`fecha_creacion`) values 
(1,4,1,1,'2022-01-27 05:50:56'),
(2,6,1,1,'2022-01-27 05:50:56'),
(3,6,1,2,'2022-01-27 07:29:05'),
(4,6,1,2,'2022-01-27 07:29:05');

/*Table structure for table `venta` */

DROP TABLE IF EXISTS `venta`;

CREATE TABLE `venta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total` int(11) DEFAULT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `venta` */

insert  into `venta`(`id`,`total`,`fecha_creacion`) values 
(1,227,'2022-01-27 05:50:56'),
(2,424,'2022-01-27 07:29:05');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


/*SENTENCIAS SOLICITADAS*/
/**PRODUCTO MAS VENDIDO*/
/*SELECT producto_id,SUM(cantidad) AS vendido FROM producto_venta GROUP BY producto_id ORDER BY vendido DESC;*/

/**PRODUCTO CON MAS STOCK*/
/*SELECT id,nombre,stock FROM producto WHERE stock = (SELECT MAX(stock) FROM producto);*/