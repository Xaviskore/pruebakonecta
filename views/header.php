<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include_once ($path.'/pruebakonecta/dirs.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Prueba Konecta</title>
        <link rel="stylesheet" href="<?php echo ASSETS_CCS;?>bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="<?php echo ASSETS_JS;?>jquery-3.4.1.min.js"></script>
        <script src="<?php echo ASSETS_JS;?>bootstrap.min.js"></script>
        <script src="<?php echo ASSETS_JS;?>myJquery.js"></script>
    </head>
    <body>
        <div class="container">
            <!-- Content here -->    
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#">Konecta</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo ROOT_PATH;?>/views/productos/list.php">Productos</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo ROOT_PATH;?>/views/venta/list.php">Venta</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo ROOT_PATH;?>/views/consultas/index.php">Consultas</a>
                    </li>
                    <!--<li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                      </a>
                      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                      </div>
                    </li>-->
                  </ul>
                </div>
            </nav>

