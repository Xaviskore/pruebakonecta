<?php
include_once '../header.php';
include_once(CONTROLLERS_PATH.'MainController.php');
$controller = new MainController();
$ventas = $controller->getSales();
?>
        <div class="col-xs-12">
            <h1>Ventas</h1>
            <div>
                <a class="btn btn-success" href="./index.php">Nueva <i class="fa fa-plus"></i></a>
            </div>
            <br>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Número</th>
                        <th>Fecha</th>
                        <th>Productos vendidos</th>
                        <th>Total</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($ventas as $venta){ ?>
                    <tr>
                        <td><?php echo $venta->id; ?></td>
                        <td><?php echo $venta->fecha_creacion; ?></td>
                        <td>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Referencia</th>
                                        <th>Cantidad</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach(explode("__", $venta->productos) as $productosConcatenados){ 
                                    $producto = explode("..", $productosConcatenados)
                                    ?>
                                    <tr>
                                        <td><?php echo $producto[0]; ?></td>
                                        <td><?php echo $producto[1]; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </td>
                        <td><?php echo $venta->total; ?></td>
                        <td><a class="btn btn-danger delVenta" href="#" data-id="<?php echo $venta->id; ?>"><i class="fa fa-trash"></i></a></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
<?php
include_once '../footer.php';
?>

