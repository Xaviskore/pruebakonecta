<?php
if(!isset($_POST["total"])) exit;

include_once '../header.php';
include_once(CONTROLLERS_PATH.'MainController.php');
$controller = new MainController();
session_start();
$total = $_POST["total"];
$controller->newSale($total,$_SESSION["carrito"]);
unset($_SESSION["carrito"]);
$_SESSION["carrito"] = [];
header("Location: ./index.php?status=1");
?>

