<?php
include_once '../header.php';
include_once(CONTROLLERS_PATH.'MainController.php');
$controller = new MainController();
if(!isset($_POST["referencia"])) return;
$referencia = $_POST["referencia"];
$producto = $controller->getProductByRef($referencia);
if($producto == new stdClass()){
    header("Location: ./index.php?status=4");
}else{
    if($producto->stock < 1){
        header("Location: ./index.php?status=5");
        exit;
    }
    session_start();
    $indice = false;
    for ($i=0; $i < count($_SESSION["carrito"]); $i++) { 
        if($_SESSION["carrito"][$i]->referencia === $codigo){
            $indice = $i;
            break;
        }
    }
    if($indice === FALSE){
        $producto->cantidad = 1;
        $producto->total = $producto->precio;
        array_push($_SESSION["carrito"], $producto);
    }else{
        $_SESSION["carrito"][$indice]->cantidad++;
        $_SESSION["carrito"][$indice]->total = $_SESSION["carrito"][$indice]->cantidad * $_SESSION["carrito"][$indice]->precio;
    }
    header("Location: ./index.php");
}
?>

