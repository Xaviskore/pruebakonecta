<?php
include_once '../header.php';
include_once(CONTROLLERS_PATH.'MainController.php');
$controller = new MainController();
$categorias = $controller->getCategories();
$productos = $controller->getProducts();
foreach($productos as $producto){
    foreach($categorias as $categoria){
        if($producto->categoria_id == $categoria['id']){
            $producto->categoria = $categoria['nombre'];
        }
    }
}
?>
	<div class="col-xs-12">
            <h1>Productos</h1>
            <div>
                <a class="btn btn-success" href="<?php echo ROOT_PATH;?>/views/productos/new.php">Nuevo <i class="fa fa-plus"></i></a>
            </div>
            <br>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Referencia</th>
                        <th>Precio</th>
                        <th>Peso</th>
                        <th>Stock</th>
                        <th>Categoria</th>
                        <th>Fecha Creación</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                    <tbody>
                        <?php foreach($productos as $producto){ ?>
                        <tr>
                            <td><?php echo $producto->id; ?></td>
                            <td><?php echo $producto->nombre; ?></td>
                            <td><?php echo $producto->referencia; ?></td>
                            <td><?php echo $producto->precio; ?></td>
                            <td><?php echo $producto->peso; ?></td>
                            <td><?php echo $producto->stock; ?></td>
                            <td><?php echo $producto->categoria; ?></td>
                            <td><?php echo $producto->fecha_creacion; ?></td>
                            <td><a class="btn btn-warning" href="<?php echo ROOT_PATH;?>/views/productos/edit.php?id=<?php echo $producto->id;?>"><i class="fa fa-edit"></i></a></td>
                            <td><a class="btn btn-danger delB" href="#" data-id="<?php echo $producto->id; ?>"><i class="fa fa-trash"></i></a></td>
                        </tr>
                        <?php } ?>
                    </tbody>
            </table>
	</div>
<?php
include_once '../footer.php';
?>
