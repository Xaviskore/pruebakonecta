<?php
include_once '../header.php';
include_once(CONTROLLERS_PATH.'MainController.php');
$controller = new MainController();
$categorias = $controller->getCategories();
?>
            <div class="col-xs-12">
                <h1>Nuevo producto</h1>
                <form id="frmProduct" action="#">
                    <label for="nombre">Nombre:</label>
                    <input class="form-control" name="nombre" required type="text" id="codigo" placeholder="Escribe el nombre">

                    <label for="referencia">Referencia:</label>
                    <input class="form-control" name="referencia" required type="text" id="codigo" placeholder="Escribe la referencia">

                    <label for="precio">Precio:</label>
                    <input class="form-control" name="precio" required type="number" id="precioVenta" placeholder="Precio">

                    <label for="peso">Peso:</label>
                    <input class="form-control" name="peso" required type="number" id="precioCompra" placeholder="Peso">
                    
                    <label for="stock">Stock:</label>
                    <input class="form-control" name="stock" required type="number" id="existencia" placeholder="Cantidad o existencia">
                    
                    <label for="categoria">Categoria:</label>
                    <select id="categoria" name="categoria_id" class="form-control" required>
                      <option selected>Seleccione...</option>
                      <?php
                      foreach($categorias as $data){
                         echo "<option value=".$data['id'].">".$data['nombre']."</option>";
                      }
                      ?>
                    </select>
                    
                    <br><br><input class="btn btn-info" type="submit" value="Guardar">
                </form>
            </div>
<?php
include_once '../footer.php';
?>
