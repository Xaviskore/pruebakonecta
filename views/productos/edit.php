<?php
include_once '../header.php';
if(!isset($_GET["id"])){
    header("Location:".ROOT_PATH."index.php");
}else{
    $id = $_GET["id"];
    include_once(CONTROLLERS_PATH.'MainController.php');
    $controller = new MainController();
    $producto = $controller->getProduct($id);
    $categorias = $controller->getCategories();
    if($producto == new stdClass()){
        echo "¡No existe algún producto con ese ID!";
        exit();
    }
}
?>
	<div class="col-xs-12">
		<h1>Editar producto ID: <?php echo $producto->id; ?></h1>
		<form id="frmProductEdit" action="#">
			<label for="nombre">Nombre:</label>
			<input value="<?php echo $producto->nombre; ?>" class="form-control" name="nombre" required type="text" id="nombre" placeholder="Escribe el nombre">
                        
                        <label for="referencia">Referencia:</label>
			<input value="<?php echo $producto->referencia; ?>" class="form-control" name="referencia" required type="text" id="nombre" placeholder="Escribe la referencia">
			
                        <label for="precio">Precio:</label>
			<input value="<?php echo $producto->precio; ?>" class="form-control" name="precio" required type="number" id="precio" placeholder="Precio">

			<label for="peso">Peso:</label>
			<input value="<?php echo $producto->peso; ?>" class="form-control" name="peso" required type="number" id="peso" placeholder="Peso">

			<label for="stock">Stock:</label>
			<input value="<?php echo $producto->stock; ?>" class="form-control" name="stock" required type="number" id="stock" placeholder="Cantidad o existencia">
			
                        <label for="categoria">Categoria:</label>
                        <select id="categoria" name="categoria_id" class="form-control" required>
                          <option>Seleccione...</option>
                            <?php foreach($categorias as $data): ?>
                            <option value="<?php echo $data['id']; ?>"<?php if($data['id'] == $producto->categoria_id): ?> selected="selected"<?php endif; ?>><?php echo $data['nombre'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        <input type="hidden" name="id" value="<?php echo $producto->id; ?>">
                        <br><br><input class="btn btn-info" type="submit" value="Guardar">
			<a class="btn btn-warning" href="list.php">Cancelar</a>
		</form>
	</div>
<?php
include_once '../footer.php';
?>
