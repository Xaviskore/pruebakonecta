<?php
include_once '../header.php';
include_once(CONTROLLERS_PATH.'MainController.php');
$controller = new MainController();
$producto1 = $controller->getPIdBySQL("SELECT producto_id,SUM(cantidad) AS vendido FROM producto_venta GROUP BY producto_id ORDER BY vendido DESC");
$producto2 = $controller->getPIdBySQL("SELECT id,nombre,stock FROM producto WHERE stock = (SELECT MAX(stock) FROM producto);");
?>
<div class="col-xs-12">
    <h3>El producto más vendido es el ID: <?php echo $producto1[0]->producto_id;?></h3>
    <h3>El producto con mayor stock es ID: <?php echo $producto2[0]->id;?> con stock <?php echo $producto2[0]->stock;?></h3>
</div>
<?php
include_once '../footer.php';
?>

