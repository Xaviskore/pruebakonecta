<?php
include_once 'controllers/MainController.php';
$contr = new MainController();
//$contr->initService();
?>
<div id="data-table" style="display: none;">
    
    <h2>Tabla archivos de webservice</h2>
    
    <table class="table" id="myTable">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Id document.</th>
            <th scope="col">Name</th>
          </tr>
        </thead>
        <tbody>         
        </tbody>
    </table>
    
    <h2>Tabla cantidad de tipos de archivos</h2>
    
    <table class="table" id="myTable2">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Type Extension</th>
            <th scope="col">Quantity</th>
          </tr>
        </thead>
        <tbody>         
        </tbody>
    </table>
    
</div>
